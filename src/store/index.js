import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    products: [],
    product: null,
    cart: [],
  },
  mutations: {
    SET_PRODUCTS: (state, products) => {
      state.products = products;
    },
    SET_PRODUCT: (state, product) => {
      state.product = product;
    },
    ADD_TO_CART: (state, { product, quantity }) => {
      // console.log('VIEW THIS ONE FIRST', product._id, product.name);
      // let productInCart = state.cart.find((item) => {
      //   console.log('DISPLAY ITEM: ', item.product._id, item.product.name);
      //   return item.product._id === product._id;
      // });

      // console.log('PRODUCTINCART DISPLAY: ', productInCart);
      // if (productInCart) {
      //   productInCart.quantity += quantity;
      //   quantity = productInCart.quantity;
      //   console.log('PRODUCTCART QUANTITY: ', productInCart.quantity);
      //   return;
      // }

      state.cart.push({
        product,
        product_name: product.name,
        product_price: product.price,
        quantity,
      });
    },
    SET_CART: (state, cartItems) => {
      state.cart = cartItems;
      console.log('STORE CART ITEMS: ', cartItems._id);
    },
    REMOVE_PRODUCT_FROM_CART: (state, product) => {
      state.cart = state.cart.filter((item) => {
        return item._id !== product;
      });
    },
    CLEAR_CART_ITEMS: (state) => {
      state.cart = [];
    },
  },
  actions: {
    getProducts: ({ commit }) => {
      axios.get('http://localhost:4000/api/posts').then((response) => {
        commit('SET_PRODUCTS', response.data);
      });
    },
    getProduct: ({ commit }, productId) => {
      axios
        .get(`http://localhost:4000/api/products/${productId}`)
        .then((response) => {
          commit('SET_PRODUCT', response.data);
        });
    },
    addProductToCart: ({ commit }, { product, quantity }) => {
      commit('ADD_TO_CART', { product, quantity });

      axios.post('http://localhost:4000/api/cart', {
        product_id: product,
        product_name: product.name,
        product_price: product.price,
        quantity,
      });
    },
    getCartItems: ({ commit }) => {
      axios.get('http://localhost:4000/api/cart').then((response) => {
        commit('SET_CART', response.data);
      });
    },
    removeProductFromCart: ({ commit }, product) => {
      commit('REMOVE_PRODUCT_FROM_CART', product);

      axios.delete(`http://localhost:4000/api/cart/${product}`);
    },
    clearCartItems: ({ commit }) => {
      commit('CLEAR_CART_ITEMS');

      axios.delete('http://localhost:4000/api/cart');
      console.log('CART ITEMS HAS BEEN CLEARED! ');
    },
  },
  getters: {
    cartTotalPrice: (state) => {
      let total = 0;

      state.cart.forEach((item) => {
        total += item.product_price * item.quantity;
      });
      return total;
    },
    cartItemCount: (state) => {
      return state.cart.length;
    },
  },
  modules: {},
});
