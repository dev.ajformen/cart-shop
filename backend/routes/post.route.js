const express = require('express');
const postRoute = express.Router();

let PostModel = require('../models/Post');
let CartModel = require('../models/Cart');
const Cart = require('../models/Cart');

//index
postRoute.route('/posts').get((req, res, next) => {
  PostModel.find((error, data) => {
    if (error) {
      return next(error);
    } else {
      res.json(data);
    }
  });
});

//create post
postRoute.route('/create-post').post((req, res, next) => {
  PostModel.create(req.body, (error, data) => {
    if (error) {
      return next(error);
    } else {
      res.json(data);
    }
  });
});

//update post
postRoute.route('/update-post/:id').post((req, res, next) => {
  PostModel.findByIdAndUpdate(
    req.params.id,
    {
      $set: req.body,
    },
    (error, data) => {
      if (error) {
        return next(error);
      } else {
        res.json(data);
        console.log('Post updated!');
      }
    },
  );
});

//delete post
postRoute.route('/deleted-post/:id').delete((req, res, next) => {
  PostModel.findByIdAndRemove(req.params.id, (error, data) => {
    if (error) {
      return next(error);
    } else {
      res.status(200).json({
        msg: data,
      });
    }
  });
});

//find products by ID
postRoute.route('/products/:id').get((req, res, next) => {
  PostModel.findById(req.params.id, (error, data) => {
    if (error) {
      return next(error);
    } else {
      res.json(data);
    }
  });
});

//create cart
postRoute.route('/cart').post((req, res, next) => {
  CartModel.create(req.body, (error, data) => {
    if (error) {
      return next(error);
    } else {
      res.json(data);
    }
  });
});

//display all carts
postRoute.route('/cart').get((req, res, next) => {
  CartModel.find((error, data) => {
    if (error) {
      return next(error);
    } else {
      res.json(data);
    }
  });
});

//delete cart by ID
postRoute.route('/cart/:id').delete((req, res, next) => {
  CartModel.findByIdAndRemove(req.params.id, (error, data) => {
    if (error) {
      return next(error);
    } else {
      res.status(200).json({
        msg: data,
      });
    }
  });
});

//delete all cart items
postRoute.route('/cart').delete((req, res, next) => {
  CartModel.deleteMany((error, data) => {
    if (error) {
      console.log('Delete Cart: ', data);
      return next(error);
    } else {
      res.json(data);
    }
  });
});

module.exports = postRoute;
