const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let cartSchema = new Schema(
  {
    product_id: {
      type: String,
    },
    product_name: {
      type: String,
    },
    product_price: {
      type: Number,
    },
    quantity: {
      type: Number,
    },
  },
  {
    collection: 'carts',
  },
);

module.exports = mongoose.model('Cart', cartSchema);
